package ru.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.godyna.tm.api.endpoint.ITaskRestEndpoint;
import ru.godyna.tm.model.Task;
import ru.godyna.tm.service.TaskService;
import ru.godyna.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @Nullable
    @GetMapping("/findAll")
    public  Collection<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findOneByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.addByUserId(task, UserUtil.getUserId());
        return task;
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.removeByUserId(task, UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(){
        taskService.removeAllByUserId(UserUtil.getUserId());
    }

}
