package ru.t1.godyna.tm.api.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.godyna.tm.model.AbstractModel;

@Repository
@Scope("prototype")
public interface IRepository <M extends AbstractModel> extends JpaRepository<M, String> {

}
